# Etude / POC RabbitMQ SSL Two Ways

## Conclusion de l'étude

### V3.6.1 (March 2016)

- Le plugin **rabbitmq_trust_store** n'est disponible qu'à partir de la version **ERLANG 17.3** (même si le contraire est indiqué dans la doc). Vérifier la version d'ERLANG pour LinkyCom
- Avertissement sur la version de ERLANG pour le TLS. la version minimum recommandé est **19.3.x**

### V3.6.4 (Juillet 2016)

Fonctionne très bien


### V3.7.0 (Novembre 2017)

Changement du format du fichier de configuration (format ini). L'ancien format (ERLANG) est aussi supporté.

Je suggère cet UPGRADE

### How to Upgrade to 3.7.0

[https://www.rabbitmq.com/upgrade.html](https://www.rabbitmq.com/upgrade.html)

## Server RabbitMQ

Le serveur RabbitMQ est démarré dans un container Docker.

La version de RabbitMQ est définit dans le `rabbit-server/Dockerfile`. Vous pouvez la modifier en cas de besoin

### Builder l'image du server

```sh
docker build --no-cache=true -t rabbitmq-ssl --build-arg RABBITMQ_VERSION=3.6.6 ./rabbit-server
```

### Exécuter le container

```sh
#créer le repertoire temporaire des certificats
mkdir -p ./certs

docker run -d --rm -p 5671:5671 -p 5672:5672 -p 15672:15672 -v ${PWD}/certs:/home/client --name rabbit-ssl rabbitmq-ssl:latest
```

### Stop the container

```sh
docker stop rabbit-ssl
```

## Client RabbitMQ

```sh
./mvnw spring-boot:run
```