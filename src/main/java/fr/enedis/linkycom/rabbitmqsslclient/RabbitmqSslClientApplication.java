package fr.enedis.linkycom.rabbitmqsslclient;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@SpringBootApplication
@Controller
public class RabbitmqSslClientApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(RabbitmqSslClientApplication.class);

	private List<SseEmitter> lsEmitters = new ArrayList<>();

	@RequestMapping(value = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	public SseEmitter stream() {
		SseEmitter emitter = new SseEmitter();
		lsEmitters.add(emitter);

		emitter.onCompletion(() -> lsEmitters.remove(emitter));
		emitter.onTimeout(() -> lsEmitters.remove(emitter));

		return emitter;
	}

	@RabbitListener(queues = { "test" })
	public void emailUpdated(Message message) {
		System.out.println("Received from RabbitMQ: " + new String(message.getBody()));
		List<SseEmitter> deadEmitters = new ArrayList<SseEmitter>();
		this.lsEmitters.forEach(emitter -> {
			try {
				emitter.send(new String(message.getBody()));
			} catch (Exception e) {
				LOGGER.error("Error ", e);
				deadEmitters.add(emitter);
			}
		});

		this.lsEmitters.removeAll(deadEmitters);
	}

	public static void main(String[] args) {
		SpringApplication.run(RabbitmqSslClientApplication.class, args);
	}
}
